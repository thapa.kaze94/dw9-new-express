import { Router } from "express";
import { createbook, deletebook, readbook, readbookDetails, updatebook } from "../../controller/bookcontroller.js";

let bookRouter=Router()
bookRouter.route("/")
.post(createbook)
.get(readbook)
bookRouter.route("/:bookId")
.patch(updatebook)
.get(readbookDetails)
.delete(deletebook)
export default bookRouter
