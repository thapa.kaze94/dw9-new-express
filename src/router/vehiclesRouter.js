import { Router } from "express";
let vRouter = Router();

vRouter.route("/") //while selecting the API it sees route ,it doesnot sees URL
  .post((req, res) => {
    //body data
    console.log('body data',req.body);
    //query data  whatver we pass in query ot return in string
    console.log('query data',req.query); 
    res.json({ success: true, message: "Vehicle asdfsadfasdfcreated successfully" });
  })
  .get((req, res) => {
    console.log(req.query);
    res.json({ success: true, message: "Vehicle read successfully" });
  })
  .patch((req, res) => {
    res.json({ success: true, message: "Vehicle updated successfully" });
  })
  .delete((req, res) => {
    res.json({ success: true, message: "Vehicle deleted successfully" });
  });

export default vRouter;
