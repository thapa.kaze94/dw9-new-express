import { Router } from "express";
import { createdepartment, deletedepartment,readdepartment, readdepartmentDetails, updatedepartment } from "../../controller/departmentController.js";
let departmentRouter=Router()
departmentRouter.route("/")
.post(createdepartment)
.get(readdepartment)
departmentRouter.route("/:departmentId")
.patch(updatedepartment)
.get(readdepartmentDetails)
.delete(deletedepartment)
export default departmentRouter
