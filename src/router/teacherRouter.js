import { Router } from "express";

import { createteacher, deleteteacher, readteacher, readteacherDetails, updateteacher } from "../../controller/teachercontroller.js";

let teacherRouter=Router()
teacherRouter.route("/")
.post(createteacher)
.get(readteacher)
teacherRouter.route("/:teacherId")
.patch(updateteacher)
.get(readteacherDetails)
.delete(deleteteacher)
export default teacherRouter
