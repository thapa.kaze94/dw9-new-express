import { Router } from "express";
let tRouter = Router();

tRouter.route("/")
  .post((req, res) => {
    res.json({ success: true, message: "tranine created successfully" });
  })
  .get((req, res) => {
    res.json({ success: true, message: "tranine read successfully" });
  })
  .patch((req, res) => {
    res.json({ success: true, message: "tranine updated successfully" });
  })
  .delete((req, res) => {
    res.json({ success: true, message: "tranine deleted successfully" });
  });

export default tRouter;
