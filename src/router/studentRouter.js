import { Router } from "express";
import { createStudent, deleteStudent, readStudent, readStudentDetails, updateStudent } from "../../controller/studentController.js";
let studentRouter=Router()
studentRouter.route("/")//localhost:8000/students
.post(createStudent)
.get(readStudent)
studentRouter.route("/:studentId")
.patch(updateStudent)
.get(readStudentDetails)
.delete(deleteStudent)


export default studentRouter