import { Router } from "express";

let bikeRouter = new Router();

bikeRouter
  .route("/")
  .post((req,res,next) => {
    // res.json("bike post");
    console.log('middleware1')
    next('a')
  },
  (err,req,res,next)=>{
    console.log('error middleware')
    next([1,2,3])
  },
  (req,res,next)=>{
    console.log('middleware2')
    next()
  })
  .get((req,res) => {
    res.json("bike get");
  })
  .patch((req,res) => {
    res.json("bike patch");
  })
  .delete((req,res) => {
    res.json("bike delete");
  })

export default bikeRouter;


