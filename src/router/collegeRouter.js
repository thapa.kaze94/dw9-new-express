import { Router } from "express";
import { createCollege, deleteCollege, readCollege, readCollegeDetails, updateCollege } from "../../controller/collegeController.js";
// import { createCollege, deleteCollege, readCollege, readCollegeDetails, updateCollege } from "../../controller/collegeController.js";

let collegeRouter=Router()
collegeRouter.route("/")//localhost:8000/colleges
.post(createCollege)
.get(readCollege)
collegeRouter.route("/:collegeId")
.patch(updateCollege)
.get(readCollegeDetails)
.delete(deleteCollege)
export default collegeRouter