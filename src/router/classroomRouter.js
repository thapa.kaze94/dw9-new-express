import { Router } from "express";
import { createClassroom, deleteClassroom, readClassroom, readClassroomDetails, updateClassroom } from "../../controller/classroomController.js";

let classroomRouter=Router()
classroomRouter.route("/")
.post(createClassroom)
.get(readClassroom)

classroomRouter.route("/:classroomId")
.patch(updateClassroom)
.get(readClassroomDetails)
.delete(deleteClassroom)


export default classroomRouter