 
/*  1.make express application 
    2.assign pot number to that application
*/
//if u send data from body always use post or patch method
import express, { json } from "express";
import firstRouter from "./src/router/firstRouter.js";
import vRouter from "./src/router/vehiclesRouter.js";
import tRouter from "./src/router/tranineRouter.js";
import bikeRouter from "./src/router/bike.js";
import connectToMongoDB from "./src/database/mongoDbcon.js";
import studentRouter from "./src/router/studentRouter.js";
import bookRouter from "./src/router/bookRouter.js";
import teacherRouter from "./src/router/teacherRouter.js";
import collegeRouter from "./src/router/collegeRouter.js";
import classroomRouter from "./src/router/classroomRouter.js";
import departmentRouter from "./src/router/departmentRouter.js";
let expressApp=express() 
expressApp.use(json()) // always place this code at top of the router
let port=8000
expressApp.listen(port,() => {
    console.log(`${port}`);
})
connectToMongoDB()
expressApp.use("/",firstRouter)
expressApp.use("/",vRouter)
expressApp.use("/tranines",tRouter)
expressApp.use("/",bikeRouter)
expressApp.use("/students",studentRouter)
expressApp.use("/books",bookRouter)
expressApp.use("/teachers",teacherRouter)
expressApp.use("/colleges",collegeRouter)
expressApp.use("/classrooms", classroomRouter)
expressApp.use("/departments",departmentRouter)


//body
//body
//query
//params
//url==>route+query==>localhost/students?name=sajan&age=24 
//route==>localhost/students (students==>route parameter)
//query==>name=sajan&age=24
//route parameter is divided into two part ==>static and dynamic route parameter
//in static we have to use same name  in route 
//in dynamic we can use any name  in route
//always put dynamic 

