import { Department } from "../schema/model.js"

export let createdepartment = (async(req,res)=>{
    let data=req.body
    let result=Department.create(data)
    try {
        let result= await Department.find({})
        res.json({
            sucess:true,
            messgae :"department created sucessfulyy",
            result:result
        })
        
    } catch (error) {
       res.json({
        success:false,
        message:error.message
       })
     }
 
})
export let readdepartment=(async(req,res)=>{
    try {
        let result= await Department.find({})
        res.json({
            sucess:true,
            messgae :"department read sucessfulyy",
            result:result
        })
        
    } catch (error) {
        res.json({
            sucess:false
        })
     }
  
})
export let updatedepartment=(async(req,res)=>{
    let departmentId=req.params.departmentId
    let data=req.body
    try {
        let result=await Department.findByIdAndUpdate(departmentId,data)
         res.json({
            sucess:true,
            messgae :"department read sucessfulyy",
            result:result
        })
      
    } catch (error) {
        res.json({
            sucess:true,
            messgae:error.messgae,
        })
        
    }
})
export let deletedepartment=(async(req,res)=>{
    let departmentId=req.params.departmentId
    let data=req.body
    try {
        let result=await Department.findByIdAndDelete(departmentId,data)
         res.json({
            sucess:true,
            messgae :"department deleted sucessfulyy",
            result:result
        })
      
    } catch (error) {
        res.json({
            sucess:true,
            messgae:error.messgae,
        })
        
    }
})

export let readdepartmentDetails = async(req,res)=>{
    let departmentId=req.params.departmentId
    try {
        let result=await Department.findById(departmentId)
         res.json({
            sucess:true,
            messgae :"department found sucessfulyy",
            result:result
        })
      
    } catch (error) {
        res.json({
            sucess:true,
            messgae:error.messgae,
        })
        
    }
}