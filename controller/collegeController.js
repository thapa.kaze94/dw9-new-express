import { College } from "../schema/model.js";
export let createCollege =(async(req,res)=>{
    try {
        let data=req.body
    let result=College.create(data)
    res.json({
        sucess:true,
        messgae :"college created sucessfulyy"
    })
    } catch (error) {
        res.json({
            sucess:false
        })
        
    }
    
})
export let readCollege=(async(req,res)=>{
    try {
        let result= await College.find({})
        res.json({
            sucess:true,
            messgae :"college read sucessfulyy",
            result:result
        })
        
    } catch (error) {
        res.json({
            sucess:false
        })
     }
  
})

export let updateCollege=(async(req,res)=>{
    let collegeId=req.params.collegeId
    let data=req.body
    try {
        let result=await College.findByIdAndUpdate(collegeId,data)
         res.json({
            sucess:true,
            messgae :"college read sucessfulyy",
            result:result
        })
      
    } catch (error) {
        res.json({
            sucess:true,
            messgae:error.messgae,
        })
        
    }
})
export let deleteCollege=(async(req,res)=>{
    let collegeId=req.params.collegeId
    let data=req.body
    try {
        let result=await College.findByIdAndDelete(collegeId,data)
         res.json({
            sucess:true,
            messgae :"college deleted sucessfulyy",
            result:result
        })
      
    } catch (error) {
        res.json({
            sucess:true,
            messgae:error.messgae,
        })
        
    }
})


export let readCollegeDetails = async(req,res)=>{
    let collegeId=req.params.collegeId
    try {
        let result=await College.findById(collegeId)
         res.json({
            sucess:true,
            messgae :"college found sucessfulyy",
            result:result
        })
      
    } catch (error) {
        res.json({
            sucess:true,
            messgae:error.messgae,
        })
        
    }
}