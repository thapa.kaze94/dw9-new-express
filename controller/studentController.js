import { Student } from "../schema/model.js"

export let createStudent =(async(req,res)=>{
    let data=req.body
    try {
    let result=await Student.create(data)
    res.json({
        sucess:true,
        messgae :"student created sucessfulyy",
        result:result
    })
    } catch (error) {
        res.json({
            sucess:false,
            message:error.message
        })
        
    }
})
export let readStudent=(async(req,res)=>{
    try {
          // let result= await Student.find({})==>find all
        // let result= await Student.find({name:"hari"})==>find name with hari
        // let result= await Student.find({age:{$gt:30}})==>find age greater than 30
        // let result= await Student.find({age:{$gte:20,  $lte:25}})==>age between 20 and 25
        //find({$or:[{name:"sajan"},{name:"hari"}])==>find name with sajan and hari
        // let result= await Student.find({name:{$in:["hari","nitan","shyam"]}})
        // let result =await Student.find({$or:[{name:"hari"},{name:"gopal"}]})
        // let result =await Student.find({$or:[{name:"hari",age:30},{name:"gopal"}]})
        // let result =await Student.find({$and:[{age:{$gte:20}},{age:{$lte:30}}]})
        // let result= await Student.find({name:{$nin:["hari"]}})
        let result= await Student.find({name:{$nin:["hari"]}})

        res.json({
            sucess:true,
            messgae :"student read sucessfully",
            result:result,
        })
    } catch (error) {
        res.json({
            sucess:false
        })
     }
  
})
export let updateStudent=(async(req,res)=>{
    let studentId=req.params.studentId
    let data=req.body
    try {
        let result=await Student.findByIdAndUpdate(studentId,data)
         res.json({
            sucess:true,
            messgae :"student read sucessfulyy",
            result:result
        })
      
    } catch (error) {
        res.json({
            sucess:true,
            messgae:error.messgae,
        })
        
    }
})
export let deleteStudent=(async(req,res)=>{
    let studentId=req.params.studentId
    let data=req.body
    try {
        let result=await Student.findByIdAndDelete(studentId,data)
         res.json({
            sucess:true,
            messgae :"student deleted sucessfulyy",
            result:result
        })
      
    } catch (error) {
        res.json({
            sucess:true,
            messgae:error.messgae,
        })
        
    }
})


export let readStudentDetails = async(req,res)=>{
    let studentId=req.params.studentId
    try {
        let result=await Student.findById(studentId)
         res.json({
            sucess:true,
            messgae :"student found sucessfulyy",
            result:result
        })
      
    } catch (error) {
        res.json({
            sucess:true,
            messgae:error.messgae,
        })
        
    }
}