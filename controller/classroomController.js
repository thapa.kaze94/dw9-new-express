import { Classroom } from "../schema/model.js"
export let createClassroom =((req,res)=>{
    let data=req.body
    let result=Classroom.create(data)
    res.json({
        sucess:true,
        messgae :"classroom created sucessfulyy"
    })
})
export let readClassroom=(async(req,res)=>{
    try {
        let result= await Classroom.find({})
        res.json({
            sucess:true,
            messgae :"classroom read sucessfulyy",
            result:result
        })
        
    } catch (error) {
        res.json({
            sucess:false
        })
     }
  
})

export let updateClassroom=(async(req,res)=>{
    let classroomId=req.params.classroomId
    let data=req.body
    try {
        let result=await Classroom.findByIdAndUpdate(classroomId,data)
         res.json({
            sucess:true,
            messgae :"classroom read sucessfulyy",
            result:result
        })
      
    } catch (error) {
        res.json({
            sucess:true,
            messgae:error.messgae,
        })
        
    }
})
export let deleteClassroom=(async(req,res)=>{
    let classroomId=req.params.classroomId
    let data=req.body
    try {
        let result=await Classroom.findByIdAndDelete(classroomId,data)
         res.json({
            sucess:true,
            messgae :"classroom deleted sucessfulyy",
            result:result
        })
      
    } catch (error) {
        res.json({
            sucess:true,
            messgae:error.messgae,
        })
        
    }
})


export let readClassroomDetails = async(req,res)=>{
    let classroomId=req.params.classroomId
    try {
        let result=await Classroom.findById(classroomId)
         res.json({
            sucess:true,
            messgae :"classroom found sucessfulyy",
            result:result
        })
      
    } catch (error) {
        res.json({
            sucess:true,
            messgae:error.messgae,
        })
        
    }
}