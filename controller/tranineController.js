import { Trainee } from "../schema/model.js";
export let createTrainee =((req,res)=>{
    let data=req.body
    let result=Trainee.create(data)
    res.json({
        sucess:true,
        messgae :"trainee created sucessfulyy",
        result:result
    })
})
export let readTrainee=(async(req,res)=>{
    try {
        let result= await Trainee.find({})
        res.json({
            sucess:true,
            messgae :"trainee read sucessfulyy",
            result:result
        })
        
    } catch (error) {
        res.json({
            sucess:false
        })
     }
  
})

export let updateTrainee=(async(req,res)=>{
    let traineeId=req.params.traineeId
    let data=req.body
    try {
        let result=await Trainee.findByIdAndUpdate(traineeId,data)
         res.json({
            sucess:true,
            messgae :"trainee read sucessfulyy",
            result:result
        })
      
    } catch (error) {
        res.json({
            sucess:true,
            messgae:error.messgae,
        })
        
    }
})
export let deleteTrainee=(async(req,res)=>{
    let traineeId=req.params.traineeId
    let data=req.body
    try {
        let result=await Trainee.findByIdAndDelete(traineeId,data)
         res.json({
            sucess:true,
            messgae :"trainee deleted sucessfulyy",
            result:result
        })
      
    } catch (error) {
        res.json({
            sucess:true,
            messgae:error.messgae,
        })
        
    }
})


export let readTraineeDetails = async(req,res)=>{
    let traineeId=req.params.traineeId
    try {
        let result=await Trainee.findById(traineeId)
         res.json({
            sucess:true,
            messgae :"trainee found sucessfulyy",
            result:result
        })
      
    } catch (error) {
        res.json({
            sucess:true,
            messgae:error.messgae,
        })
        
    }
}