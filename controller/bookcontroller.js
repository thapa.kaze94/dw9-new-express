import { Book } from "../schema/model.js"
export let createbook = (async(req,res)=>{
    let data=req.body
    let result=Book.create(data)
    try {
        let result= await Book.find({})
        res.json({
            sucess:true,
            messgae :"book created sucessfulyy"
        })
        
    } catch (error) {
       res.json({
        success:false,
        message:error.message
       })
     }
 
})
export let readbook=(async(req,res)=>{
    try {
        let result= await Book.find({})
        res.json({
            sucess:true,
            messgae :"book read sucessfulyy",
            result:result
        })
        
    } catch (error) {
        res.json({
            sucess:false
        })
     }
  
})
export let updatebook=(async(req,res)=>{
    let bookId=req.params.bookId
    let data=req.body
    try {
        let result=await Book.findByIdAndUpdate(bookId,data)
         res.json({
            sucess:true,
            messgae :"book read sucessfulyy",
            result:result
        })
      
    } catch (error) {
        res.json({
            sucess:true,
            messgae:error.messgae,
        })
        
    }
})
export let deletebook=(async(req,res)=>{
    let bookId=req.params.bookId
    let data=req.body
    try {
        let result=await Book.findByIdAndDelete(bookId,data)
         res.json({
            sucess:true,
            messgae :"book deleted sucessfulyy",
            result:result
        })
      
    } catch (error) {
        res.json({
            sucess:true,
            messgae:error.messgae,
        })
        
    }
})
export let readbookDetails = async(req,res)=>{
    let bookId=req.params.bookId
    try {
        let result=await Book.findById(bookId)
         res.json({
            sucess:true,
            messgae :"book found sucessfulyy",
            result:result
        })
      
    } catch (error) {
        res.json({
            sucess:true,
            messgae:error.messgae,
        })
        
    }
}