import { Schema } from "mongoose"
let tranineSchema=Schema({
    name:{
        type: String,
        required:[true,"name field is required"]
    },
    class:{
        type:String,
        required:[true,"class field is reuired"]
    },
    faculty:{
        type:String,
        required:[true,"faculty field is required"]
    },
})
export default tranineSchema