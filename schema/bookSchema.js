import { Schema } from "mongoose"

let bookSchema=Schema({
    name:{
        type:String,
        require:[true,"name field is required"]

    },
    author:{
        type:String,
        required:[true,"author field is required"]
    },
    price:{
        type:Number,
        required:[true,"price field is required"]

    },
    isAvailabe:{
        type:Boolean,
        required:[true,"isAvailabe field is required"]
    },
})
export default bookSchema