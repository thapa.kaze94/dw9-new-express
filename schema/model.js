import { model } from "mongoose";
import bookSchema from "./bookSchema.js";
import studentSchema from "./studentSchema.js";
import teacherSchema from "./teacherSchema.js";
import tranineSchema from "./traineeSchema.js";
import collegeSchema from "./collegeSchema.js";
import classroomSchema from "./classRoom.js";
import departmentSchema from "./departmentSchema.js";
export let Student = model("Student",studentSchema)
export let Teacher = model("Teacher",teacherSchema)
export let Book = model("Book",bookSchema)
export let Tranine =model("Tranine",tranineSchema)
export let College =model("College",collegeSchema)
export let Classroom =model("Classroom",classroomSchema)
export let Department =model("Department",departmentSchema)
