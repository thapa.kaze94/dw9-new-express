import mongoose, { Schema } from "mongoose";
let teacherSchema=Schema({
    name:{
        type:String,
        required:[true,"name field is reuqired"]
    },
    age:{
        type:Number,
        required:[true,"age field is reuqired"]
    },
    isMarried:{
        type:Boolean,
        required:[true,"ismarried field is required"]
    },
})
export default teacherSchema